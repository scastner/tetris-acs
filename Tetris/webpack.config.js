﻿var path = require('path');

module.exports = {
    entry: [
        path.join(__dirname, 'client/app.jsx'),
        path.join(__dirname, 'client/game/index.js')
    ],
    output: {
        path: path.join(__dirname, 'public'),
        filename: 'tetris.js'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['es2015', 'react']
                    }
                }
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                loader: 'style-loader!css-loader'
            }
        ]
    }
};