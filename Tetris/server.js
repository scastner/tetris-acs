// https://manage.openshift.com/

const socketio = require('socket.io');
const express = require('express');
const uniqid = require('uniqid');
const http = require('http');
const fs = require('fs');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

let scores = require('./scores.json');

app.use(express.static('public'));

io.on('connection', function (socket) {
    let s = Object.values(scores);
    s.sort((a, b) => {
        return a.score === b.score ? 0 : a.score < b.score ? 1 : -1;
    });
    io.emit('scores', s.slice(0, 15));

    socket.on('newgame', function (name) {
        socket.uid = uniqid();
        scores[socket.uid] = {
            name: name,
            score: 0
        };
        socket.emit('newgame');
    });

    socket.on('score', function (score) {
        if (scores[socket.uid]) {
            scores[socket.uid].score = score;

            let s = Object.values(scores);
            s.sort((a, b) => {
                return a.score === b.score ? 0 : a.score < b.score ? 1 : -1;
            });
            io.emit('scores', s.slice(0, 15));
        }
    });

    socket.on('disconnect', function () {
        console.log('Client disconnected.');
    });
});

setInterval(() => {
    fs.writeFile('scores.json', JSON.stringify(scores), 'utf8', () => { });
}, 1000);

const PORT = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080;
const IP = process.env.OPENSHIFT_NODEJS_IP || process.env.IP || '0.0.0.0';
server.listen(PORT, IP, () => {
    console.log(`Server listening on port ${server.address().port}`);
});