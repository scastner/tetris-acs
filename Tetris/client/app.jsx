import React, { Component, Fragment } from 'react';
import ReactDOM from 'react-dom';

import HighScores from './HighScores.jsx';
import GameCanvas from './GameCanvas.jsx';
import Info from './Info.jsx';

import "./app.css";

const socket = io();

class App extends Component {
    constructor() {
        super();

        this.state = {
            level: 1,
            score: 0,
            scores: []
        }
    }

    componentDidMount() {
        socket.on('scores', scores => {
            this.setState({ scores: scores });
        });
    }

    handleLevelChange(level) {
        this.setState({ level: level });
    }

    handleScoreChange(score) {
        this.setState({ score: score });
    }

    handleGameOver(score) {
        this.setState({ score: score });
    }

    handleNewGame(name) {
        if (name !== "") {
            socket.emit('newgame', name);
        }
    }

    render() {
        return (
            <Fragment>
                <div className="title">Tetris for Career Success</div>
                <div className="container">
                    <HighScores scores={this.state.scores} />
                    <Info
                        level={this.state.level}
                        score={this.state.score}
                        handleNewGame={name => this.handleNewGame(name)} />
                    <GameCanvas
                        socket={socket}
                        handleLevelChange={level => this.handleLevelChange(level)}
                        handleScoreChange={score => this.handleScoreChange(score)}
                        handleGameOver={score => this.handleGameOver(score)} />
                </div>
            </Fragment>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));