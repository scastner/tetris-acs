import Shape from './Shape';

class Game {
    constructor(canvas) {
        this.canvas = canvas;
        this.ctx = canvas.getContext('2d');

        this.score = 0;
        this.level = 1;

        this.rows_per_level = 10;
        this.rows_cleared_this_level = 0;

        this.onGameOverCallback = () => { };
        this.onScoreChangeCallback = () => { };
        this.onLevelChangeCallback = () => { };

        this.running = true;

        this.LOOP_INTERVAL = 1000 / 30;
        this.DELAY_INTERVAL = 1000;

        this.active_shape;
        this.cell_size = 40;
        this.grid_last = new Array(this.canvas.height / this.cell_size);
        this.grid = new Array(this.canvas.height / this.cell_size);

        for (let i = 0; i < this.grid.length; i++) {
            this.grid_last[i] = new Array(this.canvas.width / this.cell_size).fill(null);
            this.grid[i] = new Array(this.canvas.width / this.cell_size).fill(null);
        }

        this.keys = {
            left: { pressed: false, down: false },
            right: { pressed: false, down: false },
            up: { pressed: false, down: false },
            down: { pressed: false, down: false },
            space: { pressed: false, down: false }
        };
        
        window.addEventListener('keydown', event => {
            if (event.keyCode === 37 || event.which === 37) {
                this.keys.left.down = true;
                this.keys.left.pressed = true;
            }
            if (event.keyCode === 39 || event.which === 39) {
                this.keys.right.down = true;
                this.keys.right.pressed = true;
            }
            if (event.keyCode === 38 || event.which === 38) {
                this.keys.up.down = true;
                this.keys.up.pressed = true;
            }
            if (event.keyCode === 40 || event.which === 40) {
                this.keys.down.down = true;
                this.keys.down.pressed = true;
            }
            if (event.keyCode === 32 || event.which === 32) {
                this.keys.space.down = true;
                this.keys.space.pressed = true;
            }
        });

        window.addEventListener('keyup', event => {
            if (event.keyCode === 37 || event.which === 37) {
                this.keys.left.down = false;
            }
            if (event.keyCode === 39 || event.which === 39) {
                this.keys.right.down = false;
            }
            if (event.keyCode === 38 || event.which === 38) {
                this.keys.up.down = false;
            }
            if (event.keyCode === 40 || event.which === 40) {
                this.keys.down.down = false;
            }
            if (event.keyCode === 32 || event.which === 32) {
                this.keys.space.down = false;
            }
        });
        
        this.renderColors();
    }

    start() {
        this.spawnShape();

        let last_update = window.performance.now();
        let unprocessed_update = 0;
        let unprocessed_delay = 0;
        let safety = 1000;

        window.requestAnimationFrame(function loop() {
            let now = window.performance.now();
            let dt = now - last_update;
            unprocessed_update += dt;
            unprocessed_delay += dt;
            last_update = now;

            // Clear unprocessed time if it's been over 1 second
            if (unprocessed_update > safety) {
                unprocessed_update = 0;
                unprocessed_delay = 0;
            }

            // Process update every LOOP_INTERVAL milliseconds
            while (unprocessed_update >= this.LOOP_INTERVAL) {
                this.update(dt);

                for (let i in this.keys) {
                    this.keys[i].pressed = false;
                }
                unprocessed_update -= this.LOOP_INTERVAL;
            }

            // Process delayed every DELAY_INTERVAL milliseconds
            while (unprocessed_delay >= this.DELAY_INTERVAL) {
                this.delayedUpdate(dt);
                unprocessed_delay -= this.DELAY_INTERVAL;
            }

            this.render();

            if (this.running) {
                window.requestAnimationFrame(loop.bind(this));
            } else {
                this.renderColors();
                this.onGameOverCallback(this.score);
            }
        }.bind(this));
    }

    stop() {
        this.running = false;
    }

    update(dt) {
        this.grid = this.grid_last.map(row => row.slice());

        let canDrag = this.unprocessed_drag >= this.DRAG_INTERVAL;

        if (this.active_shape.landed_time === null) {
            if (this.keys.left.pressed) this.active_shape.move(this.grid, -1);
            if (this.keys.right.pressed) this.active_shape.move(this.grid, 1);
            if (this.keys.down.pressed) this.active_shape.fall(this.grid);
            if (this.keys.up.pressed) this.active_shape.rotate(this.grid, 1);
        }

        for (let y = 0; y < this.active_shape.size.height; y++) {
            for (let x = 0; x < this.active_shape.size.width; x++) {
                if (y + this.active_shape.position.y >= 0 && this.active_shape.matrix[y][x] === 1) {
                    this.grid[y + this.active_shape.position.y][x + this.active_shape.position.x] = this.active_shape.color;
                }
            }
        }
    }

    delayedUpdate(dt) {
        this.grid = this.grid_last.map(row => row.slice());

        if (!this.active_shape.landed()) this.active_shape.fall(this.grid);

        for (let y = 0; y < this.active_shape.size.height; y++) {
            for (let x = 0; x < this.active_shape.size.width; x++) {
                if (y + this.active_shape.position.y >= 0 && this.active_shape.matrix[y][x] === 1) {
                    this.grid[y + this.active_shape.position.y][x + this.active_shape.position.x] = this.active_shape.color;
                }
            }
        }

        let complete = false;
        if (this.active_shape.landed_time !== null) {
            let drop = 0;
            for (let y = this.grid.length - 1; y >= 0; y--) {
                let filled = 0;
                for (let x = 0; x < this.grid[y].length; x++) {
                    if (this.grid[y][x] !== null) filled++;
                    if (drop > 0) {
                        this.grid[y + drop][x] = this.grid[y][x];
                        this.grid[y][x] = null;
                    }
                }

                if (filled === this.grid[y].length) {
                    this.grid[y].fill(null);
                    complete = true;
                    drop += 1;
                }
            }

            if (drop === 1) {
                this.score += 40 * (this.level + 1);
            } else if (drop === 2) {
                this.score += 100 * (this.level + 1);
            } else if (drop === 3) {
                this.score += 300 * (this.level + 1);
            } else if (drop > 3) {
                this.score += 1200 * (this.level + 1);
            }

            if (drop > 0) {
                this.onScoreChangeCallback(this.score);

                this.rows_cleared_this_level += drop;
                if (this.rows_cleared_this_level >= this.rows_per_level) {
                    this.rows_cleared_this_level = 0;
                    this.DELAY_INTERVAL = Math.max(100, this.DELAY_INTERVAL - 100);
                    this.level++;

                    this.onLevelChangeCallback(this.level);
                }
            }
        }

        if (complete || this.active_shape.landed()) {
            if (this.active_shape.position.y <= 0) {
                this.running = false;
            } else {
                this.grid_last = this.grid.slice().map(row => row.slice());
                this.spawnShape();
            }
        }
    }

    render() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        this.ctx.strokeStyle = "#333";
        for (let y = 0; y < this.grid.length; y++) {
            for (let x = 0; x < this.grid[y].length; x++) {
                this.ctx.strokeRect(x * this.cell_size + 0.5, y * this.cell_size + 0.5, this.cell_size - 2, this.cell_size - 2);
                if (this.grid[y][x] !== null) {
                    this.ctx.fillStyle = this.grid[y][x];
                    this.ctx.fillRect(x * this.cell_size + 0.5, y * this.cell_size + 0.5, this.cell_size - 2, this.cell_size - 2);
                }
            }
        }

        this.ctx.strokeStyle = "#eee";
        this.ctx.strokeRect(0, 0, this.canvas.width, this.canvas.height);
    }

    renderColors() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        this.ctx.strokeStyle = "#333";
        for (let y = 0; y < this.grid.length; y++) {
            for (let x = 0; x < this.grid[y].length; x++) {
                this.ctx.strokeRect(x * this.cell_size + 0.5, y * this.cell_size + 0.5, this.cell_size - 2, this.cell_size - 2);
                let colors = Object.values(Shape.colors);
                this.ctx.fillStyle = colors[Math.floor(Math.random() * colors.length)];
                this.ctx.fillRect(x * this.cell_size + 0.5, y * this.cell_size + 0.5, this.cell_size - 2, this.cell_size - 2);
            }
        }

        this.ctx.strokeStyle = "#eee";
        this.ctx.strokeRect(0, 0, this.canvas.width, this.canvas.height);
    }

    spawnShape() {
        let forms = Object.values(Shape.forms);
        let form = forms[Math.floor(Math.random() * forms.length)];
        this.active_shape = new Shape(form, { x: 4, y: -form.length });
        this.active_shape.landed_delay = Math.max(50, 150 - 10 * this.level);
    }

    move(dir) {
        if (dir === 'left') {
            this.keys.left.pressed = true;
        } else if (dir === 'right') {
            this.keys.right.pressed = true;
        } else if (dir === 'down') {
            this.keys.down.pressed = true;
        }
    }

    rotate() {
        this.keys.up.pressed = true;
    }

    onGameOver(func) {
        this.onGameOverCallback = func;
    }

    onScoreChange(func) {
        this.onScoreChangeCallback = func;
    }

    onLevelChange(func) {
        this.onLevelChangeCallback = func;
    }
}

export default Game;