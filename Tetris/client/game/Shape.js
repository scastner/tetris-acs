class Shape {
    constructor(matrix, position) {
        this.landed_time = null;
        this.landed_delay = 150;

        this.size = {
            width: matrix[0].length,
            height: matrix.length
        };
        this.matrix = matrix;
        this.position = { x: position.x || 0, y: position.y || 0 };

        let colors = Object.values(Shape.colors);
        this.color = colors[Math.floor(Math.random() * colors.length)];
    }

    rotate(grid, dir) {
        let matrix = this.matrix.slice().map(row => row.slice());

        if (dir === -1) {
            matrix = matrix.reverse()[0].map((col, i) => matrix.map(row => row[i]));
        } else if (dir === 1) {
            matrix = matrix.map(row => row.slice().reverse());
            matrix = matrix[0].map((col, i) => matrix.map(row => row[i]));
        }

        let filled = 0;
        let moveable = 0;
        for (let y = this.size.height - 1; y >= 0; y--) {
            for (let x = 0; x < this.size.width; x++) {
                if (y + this.position.y >= 0 && matrix[y][x] === 1) {
                    if (y >= 0 && y <= grid.length && x >= 0 && x < grid[0].length && grid[y + this.position.y][x + this.position.x] === null) {
                        moveable += 1;
                    }
                    filled += 1;
                }
            }
        }

        if (filled === moveable) {
            this.matrix = matrix;
        }
    }

    move(grid, dir) {
        let filled = 0;
        let moveable = 0;
        for (let y = this.size.height - 1; y >= 0; y--) {
            for (let x = 0; x < this.size.width; x++) {
                if (y + this.position.y >= 0 && this.matrix[y][x] === 1) {
                    if (grid[y + this.position.y][x + this.position.x + dir] === null) {
                        moveable += 1;
                    }
                    filled += 1;
                }
            }
        }

        if (filled === moveable) {
            this.position.x += dir;
        }
    }

    fall(grid) {
        let filled = 0;
        let moveable = 0;
        for (let y = this.size.height - 1; y >= 0; y--) {
            for (let x = 0; x < this.size.width; x++) {
                if (y + this.position.y >= 0 && this.matrix[y][x] === 1) {
                    if (y + this.position.y + 1 < grid.length && grid[y + this.position.y + 1][x + this.position.x] === null) {
                        moveable += 1;
                    }
                    filled += 1;
                }
            }
        }

        if (filled === moveable) {
            this.position.y += 1;
        } else {
            this.landed_time = window.performance.now();
        }
    }

    landed() {
        return this.landed_time === null ? false : window.performance.now() - this.landed_time > this.landed_delay;
    }
}

Shape.colors = {
    r: "#78281F",
    g: "#186A3B",
    b: "#1B4F72",
    y: "#7D6608",
    p: "#512E5F",
    o: "#784212",
    w: "#7B7D7D"
};

Shape.forms = {
    square: [
        [1, 1],
        [1, 1]
    ],
    leftL: [
        [1, 1, 0],
        [1, 0, 0],
        [1, 0, 0]
    ],
    rightL: [
        [0, 1, 1],
        [0, 0, 1],
        [0, 0, 1]
    ],
    shapeZ: [
        [0, 1, 0],
        [1, 1, 0],
        [1, 0, 0]
    ],
    shapeS: [
        [0, 1, 0],
        [0, 1, 1],
        [0, 0, 1]
    ],
    shapeT: [
        [0, 0, 0],
        [0, 1, 0],
        [1, 1, 1]
    ],
    line: [
        [0, 0, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 1, 0]
    ]
};

export default Shape;