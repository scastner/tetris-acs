import React, { Component } from 'react';

import './highscores.css';

class HighScore extends Component {
    render() {
        return <div><span>{this.props.name}</span><span>{this.props.score}</span></div>;
    }
}

class HighScores extends Component {
    render() {
        let scores = this.props.scores.slice();
        scores.sort((a, b) => {
            return a.score === b.score ? 0 : (a.score < b.score ? 1 : -1);
        });

        return (
            <div className="scores">
                <div>High Scores</div>
                {scores.map((value, i) => <HighScore name={value.name} score={value.score} key={i} />)}
            </div>
        );
    }
}

HighScores.defaultProps = {
    scores: []
};

export default HighScores;