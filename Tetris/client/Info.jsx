import React, { Component } from 'react';

import './info.css';

class Info extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: ""
        };
    }

    render() {
        return (
            <div className="info">
                <div className="level">
                    <span>Level: </span><span>{this.props.level}</span>
                </div>
                <div className="score">
                    <span>Score: </span><span>{this.props.score}</span>
                </div>
                <div className="controls">
                    <div>Controls</div>
                    <div><i className="fa fa-arrow-circle-up"></i> - Rotate block</div>
                    <div><i className="fa fa-arrow-circle-down"></i> - Push block down</div>
                    <div><i className="fa fa-arrow-circle-left"></i> - Move block left</div>
                    <div><i className="fa fa-arrow-circle-right"></i> - Move block right</div>
                </div>
                <div className="play">
                    <input type="text" placeholder="Enter a Name" onChange={(e) => this.setState({ name: e.target.value })} />
                    <button onClick={() => this.props.handleNewGame(this.state.name)}>New Game</button>
                </div>
            </div>
        );
    }
}

export default Info;